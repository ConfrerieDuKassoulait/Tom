# Tom, a twitch bot

![status-badge](https://git.mylloon.fr/ConfrerieDuKassoulait/Tom/badges/workflows/publish.yml/badge.svg)

## Features

Here is the list of available commands by default:

> Some command have aliases, see `list` for more information

| Command  | Explanation                                                         |
| -------- | ------------------------------------------------------------------- |
| `add`    | Add command to the database (`add command_name command_message`)    |
| `remove` | Remove command from database (`remove command_name`)                |
| `list`   | Print all available commands (internal and user-defined)            |
| `edit`   | Modify a database command (`edit command_name new_command_message`) |

## Setup with Docker

- Via [docker-compose](./docker-compose.yml)
- Via command line
  ```bash
  docker run -d \
      --name="Bot-Tom" \
      git.mylloon.fr/confreriedukassoulait/tom:latest \
      --ACCESS_TOKEN="yourAccessToken" \
      --PREFIX="yourPrefix" \
      --CHANNEL="yourChannel(s)" \
      -v /here/your/path/:/app/db
  ```

## Setup Locally

- Requires Python `3.11.6`
- Install dependencies
  ```bash
  python3 -m pip install -r requirements.txt
  ```
- Rename `.envexample` file in `.env`, then open the file and complete the fields

  | Field            | Explanation                                                                                                                                |
  | ---------------- | ------------------------------------------------------------------------------------------------------------------------------------------ |
  | `ACCESS_TOKEN`   | [Access token of the bot account](https://twitchtokengenerator.com/) (be sure you selected `Bot Chat` Token)                               |
  | `PREFIX`         | Prefix used for your bot                                                                                                                   |
  | `CHANNEL`        | Twitch channel(s) where the bot will run (separate by comma)                                                                               |
  | `RIOT_CHANNEL`\* | Replace `CHANNEL` by the channel name, example: `RIOT_PONCE`, value should be your Riot ID, prefixed by the region, example: `eu/User#123` |

  > \* Available regions are: `EU`, `AP`, `NA`, `KR`

- Start bot
  ```bash
  cd src; python3 main.py
  ```
