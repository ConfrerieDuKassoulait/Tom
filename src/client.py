from twitchio import Message
from twitchio.ext import commands
from utils.commands import CommandesDB
from utils.core import load


class Client(commands.Bot):
    def __init__(self):
        self.keys = load(["ACCESS_TOKEN", "PREFIX", "CHANNEL"])
        super().__init__(
            token=self.keys["ACCESS_TOKEN"],
            prefix=self.keys["PREFIX"],
            initial_channels=self.keys["CHANNEL"],
        )

    async def event_ready(self) -> None:
        CommandesDB().creationTable()
        print(f"Logged in as {self.nick}")

    async def event_message(self, message: Message) -> None:
        if message.echo:  # messages with echo set to True are messages sent by the bot
            return

        # Let the bot know we want to handle and invoke our commands
        await self.handle_commands(message)

    async def event_command_error(self, _, error) -> None:
        if isinstance(error, commands.errors.CommandNotFound):
            # Ignore unknown commands (useful because custom commands aren't known)
            return
        raise error
