from os import environ
from sys import exit, stderr

from dotenv import load_dotenv
from twitchio.ext.commands import Bot, Command


def load(variables: list[str]) -> dict:
    """
    Load env variables

    If not found and asked for a Riot account, raise an exception
    Else exit
    """
    keys = {}
    load_dotenv()
    for var in variables:
        try:
            res = environ[var]
            if var == "CHANNEL":
                # create a list for the channels and remove blank channels and doubles
                res = list(set(res.split(",")) - {""})
            keys[var] = res
        except KeyError:

            if var.startswith("RIOT_"):
                raise Exception(f"Riot ID not set for the channel '{var}'.")
            else:
                print(
                    f"Please set the environment variable {var} (.env file supported)",
                    file=stderr,
                )
                exit(1)

    return keys


def listCommands(client: Bot) -> list[Command]:
    cogs = client.cogs.values()
    commands = []
    for cog in cogs:
        for command in cog.commands.values():
            commands.append(command)
    return commands
