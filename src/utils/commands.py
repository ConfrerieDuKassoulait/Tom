from twitchio.ext.commands import Bot
from utils.core import listCommands
from utils.db import Database


class CommandesDB(Database):
    def __init__(self):
        super().__init__(r"db/bot.sqlite")

    def creationTable(self) -> None:
        """Créer la table qui stocker les commandes."""
        requete = """
                  CREATE TABLE IF NOT EXISTS commandes (
                      name_str TEXT,
                      content_str TEXT
                  );
                  """
        self.requete(requete)

    def ajoutCommande(self, name: str, message: str) -> None:
        """Ajoute une commande."""
        requete = """
                  INSERT INTO commandes (
                      name_str,
                      content_str
                  ) VALUES (
                      ?, ?
                  );
                  """
        self.requete(requete, [name, message])

    def suppressionCommande(self, name: str) -> None:
        """Supprime une commande."""
        requete = """
                  DELETE FROM commandes
                  WHERE name_str = ?;
                  """
        self.requete(requete, name)

    def listeCommande(self) -> list[tuple]:
        """Retourne la liste des commandes."""
        return self.getResults(self.requete("SELECT * FROM commandes;"))


def existeCommande(command: str) -> tuple[bool, str | None]:
    """Vérifie qu'une commande existe dans la base de donnée."""
    commandes = CommandesDB().listeCommande()
    for commande in commandes:
        if commande[0] == command:
            return (True, commande[1])
    return (False, None)


def existeTouteCommande(client: Bot, commande: str) -> bool:
    """Vérifie qu'une commande existe dans la base de donnée et dans le bot en lui-même."""
    commandes = []
    for command in CommandesDB().listeCommande():
        commandes.append(command[0])
    for command in listCommands(client):
        commandes.append(command.name)
        if command.aliases:
            for comm in command.aliases:
                commandes.append(comm)
    return commande in commandes
