from sqlite3 import Connection, Cursor, Error, connect
from sys import stderr


class Database:
    _cursor = tuple[Cursor, int | None] | None

    def __init__(self, url: str):
        connexion = self.createConnection(url)
        if connexion is None:
            raise Exception("Can't connect to database")
        self.connexion = connexion

    def createConnection(self, path: str) -> Connection | None:
        """Connexion à une base de donnée SQLite"""
        if not self._fileExists(path):
            open(path, "x")
        connnexion = None
        try:
            connnexion = connect(path)
        except Error as e:
            print(e, file=stderr)
        return connnexion

    def _fileExists(self, path: str) -> bool:
        """Vérifie qu'un fichier existe"""
        try:
            open(path, "r")
        except FileNotFoundError:
            return False
        else:
            return True

    def requete(
        self, requete: str, valeurs: str | list | tuple | None = None
    ) -> _cursor:
        """Envoie une requête vers la base de données"""
        try:
            curseur = self.connexion.cursor()
            if valeurs:
                if type(valeurs) not in [list, tuple]:
                    valeurs = [valeurs]
                curseur.execute(requete, valeurs)
            else:
                curseur.execute(requete)
            self.connexion.commit()
            return (curseur, curseur.lastrowid)
        except Error as e:
            print(e, file=stderr)

    def getResults(self, curseur: _cursor) -> list[tuple]:
        """Affiche le résultat d'une requête"""
        tableau = []
        if curseur is None:
            return tableau
        lignes = curseur[0].fetchall()
        for ligne in lignes:
            tableau.append(ligne)
        return tableau
