from os import listdir

from client import Client

client = Client()

# Load modules
for file in listdir("modules"):
    if file.endswith(".py") and file.startswith("-") is False:
        client.load_module(f"modules.{file[:-3]}")

print("Starting..", end=" ", flush=True)
client.run()
