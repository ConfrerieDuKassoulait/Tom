from typing import cast

from twitchio import Message
from twitchio.chatter import Chatter
from twitchio.ext.commands import Bot, Cog, Context
from twitchio.ext.commands import command as new
from utils.commands import CommandesDB, existeCommande, existeTouteCommande
from utils.core import listCommands, load


def prepare(client: Bot) -> None:
    """Add the module to the client"""
    client.add_cog(Commandes(client))


class Commandes(Cog):
    # Les méthodes avec no_global_checks=Vrai nécessite des droits de modération

    def __init__(self, client: Bot):
        self.client = client
        self.prefix = load(["PREFIX"])["PREFIX"]
        self.not_a_mod = "tu n'es pas modérateur"
        self.unknown_command = "cette commande n'existe pas"

    @new(name="add", aliases=["ajout", "ajouter"], no_global_checks=True)
    async def _add(
        self, ctx: Context, name: str | None = None, message: str | None = None
    ) -> None:
        """
        Ajoute une commande de la base de donnée du bot

        `add name message`
        """
        if name is None or message is None:
            return

        author = cast(Chatter, ctx.author)
        if author.is_mod:
            if existeTouteCommande(self.client, name) is False:
                CommandesDB().ajoutCommande(name, message)
                await ctx.send(f"@{ctx.author.name}, commande {name} ajoutée !")
            else:
                await ctx.send(f"@{ctx.author.name}, cette commande existe déjà.")
        else:
            await ctx.send(f"@{ctx.author.name}, {self.not_a_mod}.")

    @new(name="remove", aliases=["delete", "suppr", "supprimer"], no_global_checks=True)
    async def _remove(self, ctx: Context, name: str | None = None) -> None:
        """
        Supprime une commande de la base de donnée du bot

        `remove name`
        """
        if name is None:
            return

        author = cast(Chatter, ctx.author)
        if author.is_mod:
            if existeCommande(name)[0]:
                CommandesDB().suppressionCommande(name)
                await ctx.send(f"@{ctx.author.name}, commande {name} supprimée !")
            else:
                await ctx.send(f"@{ctx.author.name}, {self.unknown_command}.")
        else:
            await ctx.send(f"@{ctx.author.name}, {self.not_a_mod}.")

    @new(name="list", aliases=["liste", "commands", "commandes", "help", "aide"])
    async def _list(self, ctx: Context) -> None:
        """Affiche la liste des commandes de la base de donnée du bot"""
        commandes: list[str] = [item[0] for item in CommandesDB().listeCommande()]

        author = cast(Chatter, ctx.author)
        for command in listCommands(self.client):
            name = command.name
            if command.no_global_checks and not author.is_mod:
                continue
            if command.aliases:
                name += " (alias: "
                for aliase in command.aliases:
                    name += f"{self.prefix}{aliase}, "
                name = f"{name[:-2]})"
            commandes.append(name)
        if len(commandes) > 0:
            await ctx.send(
                f"@{ctx.author.name}, liste des commandes -> "
                + self.prefix
                + f", {self.prefix}".join(commandes)
            )
        else:
            await ctx.send(
                f"@{ctx.author.name}, aucune commande enregistrée dans la base de donnée."
            )

    @new(name="edit", no_global_checks=True)
    async def _edit(
        self,
        ctx: Context,
        name: str | None = None,
        message: str | None = None,
    ) -> None:
        """
        Modifie une commande de la base de donnée du bot

        add name new_message
        """
        if name is None or message is None:
            return

        author = cast(Chatter, ctx.author)
        if author.is_mod:
            if existeCommande(name)[0]:
                CommandesDB().suppressionCommande(name)
                CommandesDB().ajoutCommande(name, message)
                await ctx.send(f"@{ctx.author.name}, commande {name} modifiée !")
            else:
                await ctx.send(f"@{ctx.author.name}, {self.unknown_command}.")
        else:
            await ctx.send(f"@{ctx.author.name}, {self.not_a_mod}.")

    @Cog.event("event_message")  # type: ignore
    async def _event_message(self, message: Message) -> None:
        if not message.content:
            return

        if message.content.startswith(self.prefix):
            # Récupère le nom de la commande
            command = existeCommande(message.content[1:].split(" ")[0])
            if command[0]:  # vérification si elle existe
                # Envois le contenu de la commande
                await message.channel.send(f"@{message.author.name}, {command[1]}")
