from datetime import timedelta
from sys import stderr

from requests import get as http_get
from requests_cache import install_cache as cacher
from twitchio.ext.commands import Bot, Cog, Context
from twitchio.ext.commands import command as new
from utils.core import load


def prepare(client: Bot) -> None:
    """Add the module to the client"""
    client.add_cog(Misc(client))


class Misc(Cog):
    # Les méthodes avec no_global_checks=Vrai nécessite des droits de modération

    def __init__(self, client: Bot):
        self.client = client
        self.prefix = load(["PREFIX"])["PREFIX"]
        self.not_a_mod = "tu n'es pas modérateur"

    @new(name="rank", aliases=["rang"])
    async def _rank(self, ctx: Context) -> None:
        """Affiche le rang du jeu actuellement joué"""
        game = (await self.client.fetch_channels([(await ctx.channel.user()).id]))[
            0
        ].game_name

        match game:
            case "VALORANT":
                try:
                    value = f"RIOT_{ctx.message.channel.name.upper()}"
                    riot_infos = load([value])[value]
                except Exception as e:
                    print(e, "Silently pass.", file=stderr)
                else:
                    region, user_id = riot_infos.split("/")
                    name, tag = user_id.split("#")

                    cacher("cache", expire_after=timedelta(minutes=30))
                    api_link = f"https://api.kyroskoh.xyz/valorant/v1/mmr/{region}/{name}/{tag}?show=combo&display=0"
                    rank = http_get(api_link).text[:-1]

                    await ctx.send(rank)
