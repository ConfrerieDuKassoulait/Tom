FROM python:3.11.6-alpine3.18

RUN apk add dumb-init

COPY requirements.txt .
RUN pip install -r requirements.txt

WORKDIR /app
COPY src .
COPY LICENSE .

CMD [ "dumb-init", "python", "-u", "./main.py" ]
